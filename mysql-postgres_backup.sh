 user=admin
 pass=rahasia
 hosts="/path/to/list/of/hosts/list.txt"
 dir=/home/backup_db/mysql/

  backup(){
   date=$(date +%Y%m%d-%H%M)
   day=$(date +%d)
   month=$(date +%m)
   year=$(date +%Y)
   if [ ! -d "$dir/$year/$month/$day/" ];
     then mkdir --parents $dir/$tahun/$month/$day;
   fi
   dump=/usr/bin/mysqldump
   $dump $dbs -u$user -p$pass -h$host -R -K --triggers 〉 $dir/$tahun/$month/$day/$dbs-$date.sql
   gzip -f $dir/$tahun/$month/$day/$dbs-$date.sql

#    postgresql backup script
#! /bin/bash
DUMPALL='/usr/bin/pg_dumpall'
PGDUMP='/usr/bin/pg_dump'
PSQL='/usr/bin/psql'

# directory to save backups in, must be rwx by postgres user
BASE_DIR='/home/backup_db/postgres'
YMD=$(date "+%Y-%m-%d-%T")
DIR="$BASE_DIR/$YMD"
mkdir -p "$DIR"
cd "$DIR"

# get list of databases in system , exclude the tempate dbs
DBS=($($PSQL --list --tuples-only |
          awk '!/template[01]/ && $1 != "|" {print $1}'))

# first dump entire postgres database, including pg_shadow etc.
$DUMPALL --column-inserts | gzip -9 > "$DIR/db.out.gz"

# next dump globals (roles and tablespaces) only
$DUMPALL --globals-only | gzip -9 > "$DIR/globals.gz"

# now loop through each individual database and backup the
# schema and data separately
for database in "${DBS[@]}" ; do
    ALL="$DIR/$database.all.backup" 
 #dump all
    $PGDUMP -Fc "$database" > "$ALL"
done

# delete backup files older than 3 days
echo deleting old backup files:
find "$BASE_DIR/" -mindepth 1 -type d -mtime +3 -print0 |
    xargs -0r rm -rfv